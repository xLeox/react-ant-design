 import React, { Component } from 'react';
 import { Link, BrowserRouter as Router, Route, Switch } from 'react-router-dom';
 import 基本使用 from './form/基本使用';
 import 表单方法调用 from './form/表单方法调用';
 import 表单方法调用class from './form/表单方法调用class';
 import 表单布局 from './form/表单布局';
 import 必选样式 from './form/必选样式';
 import 表单尺寸 from './form/表单尺寸';
 import 动态增减表单项 from './form/动态增减表单项';
 import 动态增减嵌套字段 from './form/动态增减嵌套字段';
 import 复杂的动态增减表单项 from './form/复杂的动态增减表单项';
 import 嵌套结构与校验信息 from './form/嵌套结构与校验信息';
 import 复杂一点的控件 from './form/复杂一点的控件';
 import 自定义表单控件 from './form/自定义表单控件';
 import 表单数据存储于上层组件 from './form/表单数据存储于上层组件';
 import 多表单联动 from './form/多表单联动';
 import 内联登录栏 from './form/内联登录栏';
 import 登录框 from './form/登录框';
 import 注册新用户 from './form/注册新用户';
 import 高级搜索 from './form/高级搜索';
 import 弹出层中的新建表单 from './form/弹出层中的新建表单';
 import 时间类控件 from './form/时间类控件';
 import 自行处理表单数据 from './form/自行处理表单数据';
 import 自定义校验 from './form/自定义校验';
 import 动态校验规则 from './form/动态校验规则';
 import 校验其他组件 from './form/校验其他组件';
 class App extends Component {
   render() {
     return (
        <Router>
          <div>
            <div style={{display:'flex'}}>
              <ul>
                <li><Link to="/基本使用">基本使用</Link></li>
                <li><Link to="/表单方法调用">表单方法调用</Link></li>
                <li><Link to="/表单方法调用class">表单方法调用class</Link></li>
                <li><Link to="/表单布局">表单布局</Link></li>
              </ul>
              <ul>
                <li><Link to="/必选样式">必选样式</Link></li>
                <li><Link to="/表单尺寸">表单尺寸</Link></li>
                <li><Link to="/动态增减表单项">动态增减表单项</Link></li>
                <li><Link to="/动态增减嵌套字段">动态增减嵌套字段</Link></li>
              </ul>
              <ul>
                <li><Link to="/复杂的动态增减表单项">复杂的动态增减表单项</Link></li>
                <li><Link to="/嵌套结构与校验信息">嵌套结构与校验信息</Link></li>
                <li><Link to="/复杂一点的控件">复杂一点的控件</Link></li>
                <li><Link to="/自定义表单控件">自定义表单控件</Link></li>
              </ul>
              <ul>
                <li><Link to="/表单数据存储于上层组件">表单数据存储于上层组件</Link></li>
                <li><Link to="/多表单联动">多表单联动</Link></li>
                <li><Link to="/内联登录栏">内联登录栏</Link></li>
                <li><Link to="/登录框">登录框</Link></li>
              </ul>
              <ul>
                <li><Link to="/注册新用户">注册新用户</Link></li>
                <li><Link to="/高级搜索">高级搜索</Link></li>
                <li><Link to="/弹出层中的新建表单">弹出层中的新建表单</Link></li>
                <li><Link to="/时间类控件">时间类控件</Link></li>
              </ul>
              <ul>
                <li><Link to="/自行处理表单数据">自行处理表单数据</Link></li>
                <li><Link to="/自定义校验">自定义校验</Link></li>
                <li><Link to="/动态校验规则">动态校验规则</Link></li>
                <li><Link to="/校验其他组件">校验其他组件</Link></li>
              </ul>
            </div>

            <hr />

            <Route exact path="/基本使用" component={基本使用} />
            <Route exact path="/表单方法调用" component={表单方法调用} />
            <Route exact path="/表单方法调用class" component={表单方法调用class} />
            <Route exact path="/表单布局" component={表单布局} />
            <Route exact path="/必选样式" component={必选样式} />
            <Route exact path="/表单尺寸" component={表单尺寸} />
            <Route exact path="/动态增减表单项" component={动态增减表单项} />
            <Route exact path="/动态增减嵌套字段" component={动态增减嵌套字段} />
            <Route exact path="/复杂的动态增减表单项" component={复杂的动态增减表单项} />
            <Route exact path="/嵌套结构与校验信息" component={嵌套结构与校验信息} />
            <Route exact path="/复杂一点的控件" component={复杂一点的控件} />
            <Route exact path="/自定义表单控件" component={自定义表单控件} />
            <Route exact path="/表单数据存储于上层组件" component={表单数据存储于上层组件} />
            <Route exact path="/多表单联动" component={多表单联动} />
            <Route exact path="/内联登录栏" component={内联登录栏} />
            <Route exact path="/登录框" component={登录框} />
            <Route exact path="/注册新用户" component={注册新用户} />
            <Route exact path="/高级搜索" component={高级搜索} />
            <Route exact path="/弹出层中的新建表单" component={弹出层中的新建表单} />
            <Route exact path="/时间类控件" component={时间类控件} />
            <Route exact path="/自行处理表单数据" component={自行处理表单数据} />
            <Route exact path="/自定义校验" component={自定义校验} />
            <Route exact path="/动态校验规则" component={动态校验规则} />
            <Route exact path="/校验其他组件" component={校验其他组件} />
          </div>
        </Router>
     );
   }
 }
 
 export default App;
 